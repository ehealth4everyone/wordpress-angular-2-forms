<?php

class BQ_Widget_Contact extends BQ_Widget {

    protected $data;

    protected $action;

    public function __construct() {}

    /**
     * ACTION METHODS
     */

    /**
     * Do A Widget Action
     * @param $action
     * @param $data
     */
    public function doAction($action, $data){
        // Save the passed action
        $this->action = $action;
        // Save the passed data
        $this->data = $data;
        // Determine what action happens next
        switch($action) {
            case 'submit:form' : $this->submitFormAction(); break;
        }
    }

    /**
     * Submit A Contact Form
     */
    public function submitFormAction() {
        // Create a new gump instance
        $gump = new GUMP();
        // Retrieve the context director
        $tmpl_dir = BQ_ANGULAR_DIR.'/templates';
        // Run the data through gump
        $data = $gump->sanitize($this->data);
        // Setup the validation rules
        $gump->validation_rules(array(
            'name' => 'required',
            'email' => 'required',
            'details' => 'required'
        ));
        // Setup the filter rules
        $gump->filter_rules(array(
            'name' => 'trim|sanitize_string',
            'email' => 'trim|sanitize_email',
            'phone' => 'trim|sanitize_string',
            'details' => 'sanitize_string'
        ));
        // The error flag
        $is_critical = false;
        $is_error = false;
        // If the form validated
        $all_fields = $gump->run($data);
        // If validation failed
        if ($all_fields === false) {
            // Return a successfull result
            echo json_encode(['result' => 'error', 'message' => 'validation']); exit;
        } // Otherwise the form validated correctly
        else {
            // PHPMAILER ACTION
            // Retrieve the current contact email settings
            $emails = get_field('contact_email_templates','option');
            // If a list of emails were provided
            if ($emails && is_array($emails)) {
                // Loop through each email
                foreach ($emails as $k => $email) {
                    // Create a new emailer
                    $mailer = new PHPMailer();
                    // If an email from was provided
                    $mailer->setFrom($this->getSingleFilteredValue($email['contact_email_from_address'], $all_fields), $this->getSingleFilteredValue($email['contact_email_from_name'], $all_fields));
                    // If the email details are populated
                    foreach ($email['contact_send_email_to'] as $_k => $send_to) {
                        // Populate the recipient details
                        $recipient_name = $this->getSingleFilteredValue($send_to['contact_recipient_name'], $all_fields);
                        $recipient_address = $this->getSingleFilteredValue($send_to['contact_recipient_address'], $all_fields);
                        // Set the name and the email address
                        $mailer->addAddress($recipient_address, $recipient_name);
                    }
                    // If the email details are populated
                    if (isset($email['contact_cc_email_to']) && is_array($email['contact_cc_email_to'])) {
                        // Loop through each of the cc emails
                        foreach ($email['contact_cc_email_to'] as $_k => $send_to) {
                            // Populate the recipient details
                            $recipient_name = $this->getSingleFilteredValue($send_to['contact_recipient_name'], $all_fields);
                            $recipient_address = $this->getSingleFilteredValue($send_to['contact_recipient_address'], $all_fields);
                            // Set the name and the email address
                            $mailer->addCC($this->getSingleFilteredValue($recipient_address, $all_fields), $recipient_name);
                        }
                    }
                    // If the email details are populated
                    if (isset($email['contact_bcc_email_to']) && is_array($email['contact_bcc_email_to'])) {
                        // Loop through each of the cc emails
                        foreach ($email['contact_bcc_email_to'] as $_k => $send_to) {
                            // Populate the recipient details
                            $recipient_name = $this->getSingleFilteredValue($send_to['contact_recipient_name'], $all_fields);
                            $recipient_address = $this->getSingleFilteredValue($send_to['contact_recipient_address'], $all_fields);
                            // Set the name and the email address
                            $mailer->addBCC($this->getSingleFilteredValue($recipient_address, $all_fields), $recipient_name);
                        }
                    }
                    // Setup the email contents
                    $mailer->Subject = $this->getSingleFilteredValue($email['contact_email_subject'], $all_fields, false);
                    // EMAIL HTML CONTENTS
                    // Retrieve the email from setting
                    $email_body_html = $this->getSingleFilteredValue($email['contact_email_html_content'], $all_fields, false);
                    // If the file does not exist
                    if (is_file($tmpl_dir . '/email.html.php')) {
                        // Start gathering content
                        ob_start();
                        // Include the template file
                        include($tmpl_dir . '/email.html.php');
                        // Get contents
                        $email_body_html = stripslashes(ob_get_contents());
                        // Clean up
                        ob_end_clean();
                    }
                    // Populate the
                    $mailer->Body = $email_body_html;
                    // EMAIL ALT CONTENTS
                    // Retrieve the email from setting
                    $email_body_alt = $this->getSingleFilteredValue($email['contact_email_alt_content'], $all_fields, false);
                    // If the file does not exist
                    if (is_file($tmpl_dir . '/email.alt.php')) {
                        // Start gathering content
                        ob_start();
                        // Include the template file
                        include($tmpl_dir . '/email.alt.php');
                        // Get contents
                        $email_body_alt = stripslashes(ob_get_contents());
                        // Clean up
                        ob_end_clean();
                    }
                    // If an email from was provided
                    $mailer->AltBody = $email_body_alt;
                    // If the email details are populated
                    if (isset($email['contact_email_attachments']) && is_array($email['contact_email_attachments'])) {
                        // Loop through each of the cc emails
                        foreach ($email['contact_email_attachments'] as $_k => $attachment) {
                            // Set the name and the email address
                            $mailer->addAttachment(get_attached_file($attachment['attachment']['id']));
                        }
                    }
                    // Set the email to be html
                    $mailer->isHTML(true);
                    // If the mailer worked
                    if (!$mailer->send()) {
                        $is_critical = true;
                    }
                }
            }
        }
        // If the form encountered a critical error
        if ($is_critical) {
            // Return an error
            echo json_encode(['result' => 'error']); exit;
        } // Otherwise show success
        else { echo json_encode(['result' => 'success']); exit; }
    }

    protected function getSingleFilteredValue($text, $fields, $split=true) {
        // If a list of fields was returned
        if ($fields && is_array($fields)) {
            // Loop through each provided field
            foreach ($fields as $k => $value) {
                // Update the content with any tags
                $text = str_replace('[field:'.$k.']', filter_var($value, FILTER_SANITIZE_FULL_SPECIAL_CHARS), $text);
            }
        }
        // Return the resultant text
        return $text ;
    }

    protected function getListFiltersValues($text, $fields, $split=true) {
        // Explode the provided text using new line
        $exploded = explode(PHP_EOL, $text);
        // The filtered array
        $filtered = [];
        // Loop through each provided line
        foreach ($exploded as $k => $line) {
            // Run each line through the filtered function
            $filtered[] = $this->getSingleFilteredValue($line, $fields, $split);
        }
        // Return the filtered results
        return $filtered;
    }

}