import {NgModule, Injectable} from "@angular/core";
import {AppServiceKernel} from "../app.package/app.kernal.service";
import {Http, JsonpModule, HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";

/**
 * MODULE SERVICES
 */

@Injectable()
export class ApiHelper extends AppServiceKernel {

  constructor(http: Http) { super(http); }
}

/**
 * MODULE MAIN CLASS
 */
@NgModule({
  imports: [ BrowserModule, HttpModule, JsonpModule ],
  exports: [ ],
  declarations: [],
  bootstrap: [],
  providers: [ ApiHelper ]
})
export class ApiModule { }