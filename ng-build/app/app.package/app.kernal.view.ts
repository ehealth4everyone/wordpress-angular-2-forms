
export class AppViewKernal {
  
  public steps;
  
  public form_view;
  
  public form_related;
  
  public data;
  
  
  public showStep(step, onlyone) {
    // Save the instance as self
    var self = this;
    // Reset all the current steps
    for (var s in self.steps) {
      // Set the active page to false
      if (s == step) {
        self.steps[s].active = true;
      } else if (onlyone) { self.steps[s].active = false; }
    }
  }
  
}