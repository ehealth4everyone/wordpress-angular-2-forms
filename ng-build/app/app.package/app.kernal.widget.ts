import {Input} from "@angular/core";

export class AppWidgetKernal {

  @Input('form_widget') public form_widget;

  @Input('value') public value;

  @Input('datasource') public datasource;

  protected steps;

  protected _onTouchedCallback = function(value){};

  protected _onChangeCallback = function(value){};

  public writeValue(obj:any):void {
    // Save the instance
    var self = this;
    // Update the entity object
    self.value = obj;
  }

  public registerOnChange(fn:any):void {
    this._onChangeCallback = fn;
  }

  public registerOnTouched(fn:any):void {
    this._onTouchedCallback = fn;
  }

  public showStep(step, onlyone) {
    // Save the instance as self
    var self = this;
    // Reset all the current steps
    for (var s in self.steps) {
      // Set the active page to false
      if (s == step) {
        self.steps[s].active = true;
      } else if (onlyone) { self.steps[s].active = false; }
    }
  }

}