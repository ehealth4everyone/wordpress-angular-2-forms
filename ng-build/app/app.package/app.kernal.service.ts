import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {Http, Headers} from "@angular/http";

export class AppServiceKernel {

    protected api_url = '/wp-admin/admin-ajax.php';

    constructor(
        private http: Http) {}

    public doApiReqest(widget, action, data) {
        // Save instance for later
        var self = this;
        // Create and return a new observable
        return Observable.create(function(observer) {
            // Make the request to the api
            self.http
                .post(self.api_url, {widget:widget, action:action, data:data})
                .subscribe(function(data){
                    // Attempt to delete the record
                    observer.next(data.json());
                    observer.complete();
                });
        });
    }

}