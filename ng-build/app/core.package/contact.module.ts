import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, FormGroup, Validators, FormControl, ReactiveFormsModule, NG_VALUE_ACCESSOR} from "@angular/forms";
import {NgModule, Component, Injectable, forwardRef} from "@angular/core";
import {AppWidgetKernal} from "../app.package/app.kernal.widget";
import {ApiHelper, ApiModule} from "../api.package/api.module";
import {InputMaskModule, CalendarModule} from 'primeng/primeng';
import {AppRepositoryKernal} from "../app.package/app.kernal.repository";

/**
 * ENTITY CLASSES
 */

export class CoreContactEntity {
  public name = '';
  public email = '';
  public phone = '';
  public details = '';
}

/**
 * REPOSITORY CLASSES
 */

@Injectable()
export class CoreContactRepository extends AppRepositoryKernal {

  public getEmptyEntity() {
    // Create a new contact entity
    var entity = new CoreContactEntity();
    // Return an empty entity
    return entity;
  }

  public getControlsDefaults() {
    // Save the local instance
    var self = this;
    // Return the form control
    return new FormGroup({
      name:new FormControl("", [Validators.required]),
      email:new FormControl("", [Validators.required]),
      phone:new FormControl(""),
      details:new FormControl("", [Validators.required])
    });
  }

  public doControlsCheck(form, fields) {
    // Save the local instance
    var self = this;
    // Loop through each field
    for (var i in fields) {
      // Retrieve the form control
      var field_control = form.controls[fields[i]];
      // If the field is not valid then return out
      if (!field_control.valid) { return false; }
    }
    // Return true
    return true;
  }

}

/**
 * WIDGET COMPONENTS
 */


/**
 * MAIN COMPONENTS
 */

@Component({
  selector: 'embed-contact',
  templateUrl: '/ng-app/core.package/contact.module/contact.embed.html'
})
export class CoreContactEmbedComponent extends AppWidgetKernal {

  public is_error;

  public is_success;

  public constructor(
      protected s_api:ApiHelper,
      protected r_entity:CoreContactRepository){ super(); }
  
  public ngOnInit() {
    // Save the local instance
    var self = this;
    // The view steps
    self.steps = {
      data:{active:true},
      submitted:{active:false}
    };
    // Set up the form values
    self.value = self.r_entity.getEmptyEntity();
    // Setup the view form
    self.form_widget = self.r_entity.getControlsDefaults();
  }

  protected submitWidgetForm() {
    // Save the local instance
    var self = this;
    // Clear any errors before submitting
    self.is_error = false;
    self.is_success = false;
    // If the form is NOT valid
    // Return out and do nothing
    if (self.form_widget.valid == false) { self.is_error = true; return; }
    // Call the api with our form data
    // doApiReqest(@widget, @action, @data)
    // @widget is the widget code within the plugin
    // @action is the action code for the widget
    // @data is the data to send to the widget action
    self.s_api
        .doApiReqest('contact.form', 'submit:form', self.value)
        .subscribe(function(data){
          // If the form submitted successfully to the wp plugin
          if (data.result == 'success') {
            // Show the submitted step
            self.is_success = true;
            // Set up the form values
            self.value = self.r_entity.getEmptyEntity();
          } // Otherwise show the error
          else { self.is_error = true; }
        });
  }
  
}

@NgModule({
  imports: [ BrowserModule, FormsModule, ReactiveFormsModule, ApiModule, InputMaskModule, CalendarModule ],
  exports: [ ],
  declarations: [ CoreContactEmbedComponent ],
  providers:[ CoreContactRepository ],
  bootstrap: [ CoreContactEmbedComponent ]
})
export class CoreContactModule { }