import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {CoreContactModule} from "./core.package/contact.module";

const platform = platformBrowserDynamic();

platform.bootstrapModule(CoreContactModule);